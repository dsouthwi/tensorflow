# Tensorflow-ubi9

## Description

Tensorflow based on ubi9, adapted from [upstream docker defs](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/dockerfiles/dockerfiles/devel-gpu.Dockerfile)

NB: Not all "officially supported" versions of CUDA are actually published and available.

> CUDA 11.2 does not exist for libnvinfer / tensorRT.

## Method

Rebuild CUDA images on RHEL-compat that supports LLVM-16 (required for Tensorflow compilation)
